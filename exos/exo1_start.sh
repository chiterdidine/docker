#deux conteneurs redis
docker run -d --name conteneur1 redis
docker run -d --name conteneur2 redis:6.2.8

#deux conteneurs mysql
docker run -d --name conteneur1mysql -e MYSQL_ROOT_PASSWORD=admin mysql
docker run -d --name conteneur2mysql  -e MYSQL_ROOT_PASSWORD=admin mysql:5.7.4

