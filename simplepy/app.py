import time
import os

duration = 5

# SI LA VARIABLE D'ENVIRONNEMENT EST TROUVE ON ECRASE LA VALEUR DE DURATION
if "SLEEP_DURATION" in os.environ:
    duration = int(os.environ["SLEEP_DURATION"])

while True:
    print("simple app (python) in infinite loop....")
    time.sleep(duration)
